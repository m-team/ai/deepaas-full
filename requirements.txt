# DEEPaaS framework requirements
webargs~=5.5.2
deepaas~=2.1.0

# TODO: Edit Model requirements
dvc~=2.58.2
dvc-webdav~=2.19.1
tensorflow~=2.12.0
mlflow~=2.3.2